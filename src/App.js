import React from 'react';
import Button from '@material-ui/core/Button';
import './App.css';
import FamilyTree from './FamilyTree';
import SearchPage from './SearchPage';

const App = () => {
  const [startTree, setStartTree] = React.useState(false);
  const [userAction, setUserAction] = React.useState('create');
  const [id, setId] = React.useState('');
  const [familyTreeData, setFamilyTreeData] = React.useState({});
  const [noOfPerson, setNoOfPerson] = React.useState(0);
  const [verificationDetails, setVerificationDetails] = React.useState({});

  const handleStartTree = () => setStartTree(true);
  const handleCloseTree = () => {
    setStartTree(false);
    setUserAction('create');
    setId('');
    setFamilyTreeData({});
    setNoOfPerson(0);
    setVerificationDetails({});
  };

  const handleViewOfFamilyData = familyData => {
    setStartTree(true);
    setUserAction('view');
    setFamilyTreeData(familyData.familyTreeData);
    setNoOfPerson(familyData.noOfPerson);
    setVerificationDetails({
      primaryMail: familyData.primaryMail,
      uniqueCode: familyData.uniqueCode
    });
    setId(familyData._id);
  };

  return (
    <div className="App">
      {!startTree && (
        <div className="container">
          <Button variant="outlined" color="primary" onClick={handleStartTree}>
            START FAMILY TREE
          </Button>
          <SearchPage
            handleStartTree={handleStartTree}
            handleViewOfFamilyData={handleViewOfFamilyData}
          />
        </div>
      )}
      {startTree && (
        <FamilyTree
          handleCloseTree={handleCloseTree}
          userAction={userAction}
          familyTreeData={familyTreeData}
          verificationDetails={verificationDetails}
          id={id}
          noOfPerson={noOfPerson}
        />
      )}
    </div>
  );
};

export default App;

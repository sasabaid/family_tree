import { put, call } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* saveFamilyDataSaga(action) {
  try {
    const api = '/familydata';
    const response = yield call(callFetchApi, api, {}, 'POST', action.payload);
    yield put({
      type: actions.SAVE_FAMILY_TREE_SUCCESS,
      response: response.data
    });
  } catch (error) {
    yield put({
      type: actions.SAVE_FAMILY_TREE_FAILURE,
      error
    });
  }
}

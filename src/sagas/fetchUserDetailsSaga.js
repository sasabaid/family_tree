import { put, call } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* fetchUserDetailsSaga(action) {
  try {
    const api = '/api/user';
    const data = {
      emailaddr: action.userEmail
    };
    const response = yield call(callFetchApi, api, data, 'GET');
    yield put({
      type: actions.USER_FETCH_SUCCESS,
      response: response.data
    });
  } catch (error) {
    yield put({
      type: actions.USER_FETCH_FAILURE,
      error
    });
  }
}

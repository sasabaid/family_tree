import { takeLatest } from 'redux-saga/effects';
import actions from '../actionTypes';
import fetchUserDetailsSaga from './fetchUserDetailsSaga';
import saveFamilyDataSaga from './saveFamilyDataSaga';
import searchFamilyTreeSaga from './searchFamilyTreeSaga';
import updateFamilyDataSaga from './updateFamilyDataSaga';
import deleteFamilyTreeSaga from './deleteFamilyTreeSaga';
import saveProfileImageSaga from './saveProfileImageSaga';

export default function* saga() {
  yield takeLatest(actions.USER_FETCH, fetchUserDetailsSaga);
  yield takeLatest(actions.SAVE_FAMILY_TREE, saveFamilyDataSaga);
  yield takeLatest(actions.SEARCH_FAMILY_DATA, searchFamilyTreeSaga);
  yield takeLatest(actions.UPDATE_FAMILY_TREE, updateFamilyDataSaga);
  yield takeLatest(actions.DELETE_FAMILY_TREE, deleteFamilyTreeSaga);
  yield takeLatest(actions.SAVE_PROFILE_IMAGE, saveProfileImageSaga);
}

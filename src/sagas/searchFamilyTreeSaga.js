import { put, call } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* searchFamilyTreeSaga(action) {
  try {
    const api = `/familydata/search`;
    const data = {
      searchData: action.payload
    };
    const response = yield call(callFetchApi, api, data, 'GET');
    yield put({
      type: actions.SEARCH_FAMILY_DATA_SUCCESS,
      response: response.data
    });
  } catch (error) {
    yield put({
      type: actions.SEARCH_FAMILY_DATA_FAILURE,
      error
    });
  }
}

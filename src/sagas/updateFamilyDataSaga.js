import { put, call } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* updateFamilyDataSaga(action) {
  try {
    const api = `/familydata/${action.payload.id}`;
    const response = yield call(callFetchApi, api, {}, 'PUT', action.payload.modifiedInfo);
    yield put({
      type: actions.UPDATE_FAMILY_TREE_SUCCESS,
      response: response.data
    });
  } catch (error) {
    yield put({
      type: actions.UPDATE_FAMILY_TREE_FAILURE,
      error
    });
  }
}

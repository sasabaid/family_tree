import { put, call } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* deleteFamilyTreeSaga(action) {
  try {
    const api = `/familydata/${action.payload.id}`;
    const response = yield call(callFetchApi, api, {}, 'DELETE');
    yield put({
      type: actions.DELETE_FAMILY_TREE_SUCCESS,
      response: response.data
    });
  } catch (error) {
    yield put({
      type: actions.DELETE_FAMILY_TREE_FAILURE,
      error
    });
  }
}

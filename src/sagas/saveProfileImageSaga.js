import { put, call } from 'redux-saga/effects';
import { callFetchApi } from '../services/api';
import actions from '../actionTypes';

export default function* saveProfileImageSaga(action) {
  try {
    const api = '/uploadProfilePic';
    const data = new FormData();
    data.append('file', action.image);

    const response = yield call(callFetchApi, api, {}, 'POST', data);
    yield put({
      type: actions.SAVE_PROFILE_IMAGE_SUCCESS,
      response: response.data
    });
  } catch (error) {
    yield put({
      type: actions.SAVE_PROFILE_IMAGE_FAILURE,
      error
    });
  }
}

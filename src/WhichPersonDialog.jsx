import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const WhichPersonDialog = props => {

  return (
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Gender of the Person?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Please select the gender of the person whose details you gonna enter.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => props.handleGender("male")} color="primary">
            Male
          </Button>
          <Button onClick={() => props.handleGender("female")} color="primary" autoFocus>
            Female
          </Button>
        </DialogActions>
      </Dialog>
  );
}

export default WhichPersonDialog;
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import WhichPersonDialog from './WhichPersonDialog';
import LevelInfo from './LevelInfo';
import * as R from 'ramda';
import { countOccurencesOfCharInString } from './utils/GeneralUtils';
import Button from '@material-ui/core/Button';
import PersonDetails from './PersonDetails';
import ConfirmationDialog from './ConfirmationDialog';
import VerificationDialog from './VerificationDialog';
import actions from './actionTypes';
import htmlToImage from 'html-to-image';
const uuid = require('uuid/v4');

const FamilyTree = props => {
  const dispatch = useDispatch();

  const [userAction, setUserAction] = React.useState('');

  const [openWhichPersonDialog, setOpenWhichPersonDialog] = React.useState(false);
  const [openPersonDetailsDialog, setOpenPersonDetailsDialog] = React.useState(false);
  const [openConfirmationDialog, setOpenConfirmationDialog] = React.useState(false);
  const [openVerificationDialog, setOpenVerificationDialog] = React.useState(false);
  const [idOfPersonWhenGettingDetails, setIdOfPersonWhenGettingDetails] = React.useState(null);
  const [
    filledInDetailsOfPersonWhileEditing,
    setFilledInDetailsOfPersonWhileEditing
  ] = React.useState(undefined);
  const [noOfPerson, setNoOfPerson] = React.useState(0);
  const [idOfParentWhenGettingGender, setIdOfParentWhenGettingGender] = React.useState(null);
  const [familyTreeData, setFamilyTreeData] = React.useState({});

  useEffect(() => {
    if (props.userAction !== 'view') setOpenWhichPersonDialog(true);
    setUserAction(props.userAction);
    setFamilyTreeData(props.familyTreeData);
    setNoOfPerson(props.noOfPerson);
    // eslint-disable-next-line
  }, []);

  const getId = (type, id) => {
    switch (type) {
      case 'spouse':
        return `${id}-spouse`;
      case 'father':
        return `${id}-father`;
      case 'mother':
        return `${id}-mother`;
      case 'children':
        const noOfChildrenPresent = familyTreeData[id]['noOfChildren'];
        const idSplit = id.split('-');
        const nextCharacter = String.fromCharCode(idSplit.pop()[0].charCodeAt(0) + 1);
        return `${id}-${nextCharacter}${noOfChildrenPresent + 1}`;
      default:
        return 'A1';
    }
  };

  const handleCloseWhichPersonDialog = () => {
    if (noOfPerson === 0) props.handleCloseTree();
    setOpenWhichPersonDialog(false);
  };

  const handleGenderSelection = genderSelected => {
    let id, type;
    let familyTreeDataCopy = R.clone(familyTreeData);

    if (noOfPerson === 0) {
      type = 'head';
      id = getId(type, idOfParentWhenGettingGender);
    } else {
      type = 'children';
      id = getId(type, idOfParentWhenGettingGender);
      familyTreeDataCopy[idOfParentWhenGettingGender]['children'].push(id);
      familyTreeDataCopy[idOfParentWhenGettingGender]['noOfChildren'] =
        familyTreeDataCopy[idOfParentWhenGettingGender]['noOfChildren'] + 1;
    }

    const levelInfo = `level-${countOccurencesOfCharInString(id, '-')}`;
    const childInfo = {
      id,
      type,
      gender: genderSelected,
      level: levelInfo,
      parentId: idOfParentWhenGettingGender,
      children: [],
      noOfChildren: 0,
      showChildren: true
    };
    familyTreeDataCopy[id] = childInfo;

    setFamilyTreeData(familyTreeDataCopy);
    setNoOfPerson(noOfPerson + 1);
    setOpenWhichPersonDialog(false);
  };

  const handleChildren = parentId => {
    setIdOfParentWhenGettingGender(parentId);
    setOpenWhichPersonDialog(true);
  };

  const handleSpouse = (currentSpouseGender, husbandOrWifeId) => {
    const spouseGender = currentSpouseGender === 'male' ? 'female' : 'male';
    const id = getId('spouse', husbandOrWifeId);
    let familyTreeDataCopy = R.clone(familyTreeData);

    const levelInfo = `level-${countOccurencesOfCharInString(id, '-') - 1}`;
    const spouseInfo = {
      id,
      type: 'spouse',
      gender: spouseGender,
      level: levelInfo,
      husbandOrWifeId
    };

    const husbandOrWifeInfo = familyTreeDataCopy[husbandOrWifeId];
    husbandOrWifeInfo['spouse'] = id;
    familyTreeDataCopy[id] = spouseInfo;

    setFamilyTreeData(familyTreeDataCopy);
    setNoOfPerson(noOfPerson + 1);
  };

  const handleParent = (gender, childrenId) => {
    const typeOfParent = gender === 'male' ? 'father' : 'mother';
    const id = getId(typeOfParent, childrenId);
    let familyTreeDataCopy = R.clone(familyTreeData);

    const levelInfo = `level-${countOccurencesOfCharInString(id, '-') - 2}-1`;
    const spouseParentInfo = {
      id,
      type: typeOfParent,
      gender,
      level: levelInfo,
      childrenId
    };

    const childrenInfo = familyTreeDataCopy[childrenId];
    childrenInfo[typeOfParent] = id;
    familyTreeDataCopy[id] = spouseParentInfo;

    setFamilyTreeData(familyTreeDataCopy);
    setNoOfPerson(noOfPerson + 1);
  };

  const cardClickedForAddingEditingInfo = id => {
    setOpenPersonDetailsDialog(true);
    setIdOfPersonWhenGettingDetails(id);
    setFilledInDetailsOfPersonWhileEditing(R.clone(familyTreeData[id].personalDetails));
  };

  const removeChildTree = deletePersonId => {
    let familyTreeDataCopy = R.clone(familyTreeData);

    let childrenArray = [deletePersonId];

    familyTreeDataCopy[familyTreeDataCopy[deletePersonId].parentId]['children'] = R.without(
      [deletePersonId],
      familyTreeDataCopy[familyTreeDataCopy[deletePersonId].parentId]['children']
    );

    while (childrenArray.length !== 0) {
      let id = childrenArray.pop();
      let personinfo = R.clone(familyTreeDataCopy[id]);

      if (personinfo['children'] !== undefined) childrenArray.push(...personinfo['children']);

      if (personinfo.spouse !== undefined) {
        let spouseInfo = familyTreeDataCopy[personinfo.spouse];
        if (spouseInfo.mother !== undefined) delete familyTreeDataCopy[spouseInfo.mother];
        if (spouseInfo.father !== undefined) delete familyTreeDataCopy[spouseInfo.father];
        delete familyTreeDataCopy[personinfo.spouse];
      }
      delete familyTreeDataCopy[id];
    }

    setFamilyTreeData(familyTreeDataCopy);
  };

  const deleteInfo = id => {
    let personinfo = familyTreeData[id];
    if (personinfo.type === 'head') {
      let familyTreeDataCopy = R.clone(familyTreeData);
      delete familyTreeDataCopy[id];
      setFamilyTreeData(familyTreeDataCopy);
      setNoOfPerson(0);
      if (userAction === 'create') props.handleCloseTree();
    } else if (personinfo.type === 'children') {
      removeChildTree(id);
    } else if (personinfo.type === 'spouse') {
      let familyTreeDataCopy = R.clone(familyTreeData);
      familyTreeDataCopy[personinfo.husbandOrWifeId][personinfo.type] = undefined;
      if (personinfo.mother !== undefined) delete familyTreeDataCopy[personinfo.mother];
      if (personinfo.father !== undefined) delete familyTreeDataCopy[personinfo.father];
      delete familyTreeDataCopy[id];
      setFamilyTreeData(familyTreeDataCopy);
    } else {
      let familyTreeDataCopy = R.clone(familyTreeData);
      familyTreeDataCopy[personinfo.childrenId][personinfo.type] = undefined;
      delete familyTreeDataCopy[id];
      setFamilyTreeData(familyTreeDataCopy);
    }
  };

  // TODO: Handling error of profile image save
  const saveProfileImage = image =>
    dispatch({
      type: actions.SAVE_PROFILE_IMAGE,
      image
    });

  const handleSaveOfPersonDetails = (id, { personDetails, image }) => {
    let familyTreeDataCopy = R.clone(familyTreeData);
    familyTreeDataCopy[id]['personalDetails'] = personDetails;
    if (image) {
      let imageName = `${uuid()}||${image.name}`;
      let blob = image.slice(0, image.size, image.type);
      let newFile = new File([blob], imageName, { type: image.type });
      familyTreeDataCopy[id]['personalDetails'].profilePic = imageName;
      if (familyTreeDataCopy[id]['personalDetails'].images) {
        familyTreeDataCopy[id]['personalDetails'].images.push(imageName);
      } else {
        familyTreeDataCopy[id]['personalDetails'].images = [imageName];
      }
      saveProfileImage(newFile);
    }
    setFamilyTreeData(familyTreeDataCopy);
    setOpenPersonDetailsDialog(false);
    setIdOfPersonWhenGettingDetails(null);
    setFilledInDetailsOfPersonWhileEditing(undefined);
  };

  const hideChildren = id => {
    let familyTreeDataCopy = R.clone(familyTreeData);
    familyTreeDataCopy[id].showChildren = !familyTreeDataCopy[id].showChildren;
    setFamilyTreeData(familyTreeDataCopy);
  };

  const handleCloseOfConfirmationDialog = () => {
    setOpenConfirmationDialog(false);
  };

  const handleSaveOfCompleteTree = confirmationDetails => {
    const payload = {
      familyTreeData,
      noOfPerson,
      ...confirmationDetails
    };
    dispatch({
      type: actions.SAVE_FAMILY_TREE,
      payload
    });
    setOpenConfirmationDialog(false);
    props.handleCloseTree();
  };

  const saveFamilyTreeAsImage = () => {
    htmlToImage.toJpeg(document.getElementById('familyTreeUi')).then(function(dataUrl) {
      var link = document.createElement('a');
      link.download = 'MyFamilyTree.png';
      link.href = dataUrl;
      link.click();
    });
  };

  const handleSavingOfTree = () => {
    if (userAction === 'create') {
      setOpenConfirmationDialog(true);
    } else {
      if (familyTreeData['A1'] === undefined) {
        const payload = {
          id: props.id
        };
        dispatch({
          type: actions.DELETE_FAMILY_TREE,
          payload
        });
        props.handleCloseTree();
      } else {
        const payload = {
          modifiedInfo: {
            familyTreeData,
            noOfPerson
          },
          id: props.id
        };
        dispatch({
          type: actions.UPDATE_FAMILY_TREE,
          payload
        });
        setUserAction('view');
      }
    }
  };

  const handleVerificationForEditing = () => {
    setUserAction('edit');
    setOpenVerificationDialog(false);
  };
  const handleCloseOfVerificationDialog = () => {
    setOpenVerificationDialog(false);
  };

  return (
    <>
      {noOfPerson === 0 && userAction === 'edit' && (
        <div className={'saveTreeButtonDiv'}>
          <Button
            variant="outlined"
            color="primary"
            onClick={() => props.handleCloseTree()}
            className={'saveTreeButton'}
          >
            BACK
          </Button>
          {userAction !== 'view' && (
            <Button
              variant="outlined"
              color="primary"
              onClick={handleSavingOfTree}
              className={'saveTreeButton'}
            >
              SAVE THE DATA
            </Button>
          )}
        </div>
      )}
      {noOfPerson > 0 && (
        <div className={'saveTreeButtonDiv'}>
          <Button
            variant="outlined"
            color="primary"
            onClick={() => props.handleCloseTree()}
            className={'saveTreeButton'}
          >
            BACK
          </Button>
          {userAction !== 'view' && (
            <Button
              variant="outlined"
              color="primary"
              onClick={handleSavingOfTree}
              className={'saveTreeButton'}
            >
              SAVE THE DATA
            </Button>
          )}
          {userAction === 'view' && (
            <Button
              variant="outlined"
              color="primary"
              onClick={() => setOpenVerificationDialog(true)}
              className={'saveTreeButton'}
            >
              EDIT THE DATA
            </Button>
          )}
          <Button
            variant="outlined"
            color="primary"
            onClick={saveFamilyTreeAsImage}
            className={'saveTreeButton'}
          >
            FAMILY TREE AS IMAGE
          </Button>
        </div>
      )}
      {openConfirmationDialog && (
        <ConfirmationDialog
          handleSave={handleSaveOfCompleteTree}
          handleClose={handleCloseOfConfirmationDialog}
          familyDetails={familyTreeData}
          open={openConfirmationDialog}
        />
      )}
      {openVerificationDialog && (
        <VerificationDialog
          handleVerification={handleVerificationForEditing}
          handleClose={handleCloseOfVerificationDialog}
          verificationDetails={props.verificationDetails}
          open={openVerificationDialog}
        />
      )}
      {openWhichPersonDialog && (
        <WhichPersonDialog
          handleClose={handleCloseWhichPersonDialog}
          handleGender={handleGenderSelection}
          open={openWhichPersonDialog}
        />
      )}
      {openPersonDetailsDialog && (
        <PersonDetails
          handleSave={handleSaveOfPersonDetails}
          personId={idOfPersonWhenGettingDetails}
          personDetails={filledInDetailsOfPersonWhileEditing}
          open={openPersonDetailsDialog}
          userAction={userAction}
        />
      )}
      {noOfPerson > 0 && (
        <div id={'familyTreeUi'}>
          <LevelInfo
            handleSpouse={handleSpouse}
            handleChildren={handleChildren}
            handleParent={handleParent}
            cardClickedForAddingEditingInfo={cardClickedForAddingEditingInfo}
            deleteInfo={deleteInfo}
            hideChildren={hideChildren}
            familyTreeData={familyTreeData}
            ids={['A1']}
            userAction={userAction}
          />
        </div>
      )}
    </>
  );
};

export default FamilyTree;

import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import * as R from 'ramda';
import { doPatternTestForInputField } from './utils/GeneralUtils';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: 200,
    height: 70
  }
}));

const ConfirmationDialog = props => {
  const [primaryMail, setPrimaryMail] = React.useState('');
  const [uniqueCode, setUniqueCode] = React.useState('');
  const [saveEnabled, setSaveEnabled] = React.useState(true);
  const [error, setError] = React.useState({
    primaryMail: false,
    uniqueCode: false
  });

  useEffect(() => {
    if (
      props.familyDetails['A1'].personalDetails !== undefined &&
      props.familyDetails['A1'].personalDetails.email !== undefined
    ) {
      setPrimaryMail(props.familyDetails['A1'].personalDetails.email);
    }
    // eslint-disable-next-line
  }, []);

  const classes = useStyles();

  const handleReset = () => {
    setPrimaryMail('');
    setUniqueCode('');
    setError({
      primaryMail: false,
      uniqueCode: false
    });
    setSaveEnabled(true);
  };

  const handleSave = () => {
    let confirmationDetails = {
      primaryMail,
      uniqueCode
    };
    props.handleSave(confirmationDetails);
  };
  const pattern = {
    primaryMail: '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$',
    uniqueCode: '^[A-Za-z0-9]{6,}$'
  };

  const requiredFields = ['primaryMail', 'uniqueCode'];

  const handleOnBlur = event => {
    let errorCopy = R.clone(error);
    if (R.contains(event.target.id, requiredFields)) {
      if (event.target.value.length > 0) {
        let validInput = doPatternTestForInputField(event, pattern[event.target.id]);
        errorCopy[event.target.id] = !validInput;
      } else {
        errorCopy[event.target.id] = true;
      }
    } else {
      if (event.target.value.length > 0) {
        let validInput = doPatternTestForInputField(event, pattern[event.target.id]);
        errorCopy[event.target.id] = !validInput;
      }
    }
    setError(errorCopy);
    setSaveEnabled(R.isEmpty(R.pickBy((v, k) => v, errorCopy)));
  };

  return (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{'Confirmation Details'}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Please note the below fields are required for securing your information. Unique Code will
          be used for editing the information in future and this code will be sent to you in the
          primary email mentioned below.
        </DialogContentText>
        <form className={classes.container} autoComplete="off">
          <div>
            <TextField
              id="primaryMail"
              required
              error={error['primaryMail']}
              className={classes.textField}
              label="Primary Email"
              margin="normal"
              value={primaryMail}
              onChange={event => setPrimaryMail(event.target.value)}
              onBlur={e => handleOnBlur(e)}
              onKeyUp={e => handleOnBlur(e)}
              autoComplete="off"
              helperText={
                error['primaryMail'] && primaryMail.length !== 0 ? 'Should contain valid email' : ''
              }
            />
          </div>
          <div>
            <TextField
              id="uniqueCode"
              required
              error={error['uniqueCode']}
              className={classes.textField}
              label="Unique Code"
              margin="normal"
              value={uniqueCode}
              onChange={event => setUniqueCode(event.target.value)}
              onBlur={e => handleOnBlur(e)}
              onKeyUp={e => handleOnBlur(e)}
              autoComplete="off"
              helperText={
                error['uniqueCode'] && primaryMail.length !== 0
                  ? 'Should contain atleast 6 characters'
                  : ''
              }
            />
          </div>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleReset} color="primary">
          RESET
        </Button>
        <Button onClick={handleSave} color="primary" autoFocus disabled={!saveEnabled}>
          SAVE
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmationDialog;

import actions from '../actionTypes';

const defaultState = {
  familyData: {},
  updatedFamilyData: {},
  deletedFamilyData: {},
  saveFamilyTreeDataInProgress: false,
  updateFamilyTreeDataInProgress: false,
  deleteFamilyTreeDataInProgress: false,
  searchedFamilyData: []
};

const familyDataReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actions.SAVE_FAMILY_TREE:
      return Object.assign({}, state, { saveFamilyTreeDataInProgress: true });
    case actions.SAVE_FAMILY_TREE_SUCCESS:
      return Object.assign(
        {},
        state,
        { familyData: action.response },
        { saveFamilyTreeDataInProgress: false }
      );
    case actions.SAVE_FAMILY_TREE_FAILURE:
      return Object.assign(
        {},
        state,
        { familyData: action.error },
        { saveFamilyTreeDataInProgress: false }
      );
    case actions.UPDATE_FAMILY_TREE:
      return Object.assign({}, state, { updateFamilyTreeDataInProgress: true });
    case actions.UPDATE_FAMILY_TREE_SUCCESS:
      return Object.assign(
        {},
        state,
        { updatedFamilyData: action.response },
        { updateFamilyTreeDataInProgress: false }
      );
    case actions.UPDATE_FAMILY_TREE_FAILURE:
      return Object.assign(
        {},
        state,
        { updatedFamilyData: action.error },
        { updateFamilyTreeDataInProgress: false }
      );
    case actions.DELETE_FAMILY_TREE:
      return Object.assign({}, state, { deleteFamilyTreeDataInProgress: true });
    case actions.DELETE_FAMILY_TREE_SUCCESS:
      return Object.assign(
        {},
        state,
        { deletedFamilyData: action.response },
        { deleteFamilyTreeDataInProgress: false }
      );
    case actions.DELETE_FAMILY_TREE_FAILURE:
      return Object.assign(
        {},
        state,
        { deletedFamilyData: action.error },
        { deleteFamilyTreeDataInProgress: false }
      );
    case actions.SEARCH_FAMILY_DATA_SUCCESS:
      return Object.assign({}, state, { searchedFamilyData: action.response });
    case actions.SEARCH_FAMILY_DATA_FAILURE:
      return Object.assign({}, state, { searchedFamilyData: [] });
    case actions.SEARCH_FAMILY_DATA_RESET:
      return Object.assign({}, state, { searchedFamilyData: [] });
    default:
      return state;
  }
};

export default familyDataReducer;

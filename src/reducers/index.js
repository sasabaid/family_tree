import { combineReducers } from 'redux';
import fetchMeReducer from './fetchMeReducer';
import familyDataReducer from './familyDataReducer';
import saveProfileImage from './saveProfileImage';

const reducers = combineReducers({
  fetchMeReducer,
  familyDataReducer,
  saveProfileImage
});

export default reducers;

import actions from '../actionTypes';

const defaultState = {
  user: {},
  fetchMeInProgress: false
};

const fetchMeReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actions.FETCH_ME_SUCCESS:
      return Object.assign({}, state, { user: action.response }, { fetchMeInProgress: false });
    case actions.FETCH_ME_FAILURE:
      return Object.assign({}, state, { error: action.error }, { fetchMeInProgress: false });
    default:
      return state;
  }
};

export default fetchMeReducer;

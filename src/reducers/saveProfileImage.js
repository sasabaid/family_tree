import actions from '../actionTypes';

const defaultState = {
  error: '',
  saveProfileImageInProgress: false
};

const saveProfileImage = (state = defaultState, action) => {
  switch (action.type) {
    case actions.SAVE_PROFILE_IMAGE:
      return Object.assign({}, state, { saveProfileImageInProgress: true });
    case actions.SAVE_PROFILE_IMAGE_SUCCESS:
      return Object.assign({}, state, { saveProfileImageInProgress: false });
    case actions.SAVE_PROFILE_IMAGE_FAILURE:
      return Object.assign({}, state, { error: action.error, saveProfileImageInProgress: false });
    default:
      return state;
  }
};

export default saveProfileImage;

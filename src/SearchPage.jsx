import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import AddIcon from '@material-ui/icons/Add';
import actions from './actionTypes';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '95%'
  },
  form: {
    width: '95%'
  }
}));

const SearchPage = props => {
  const dispatch = useDispatch();

  const { searchedFamilyData } = useSelector(state => state.familyDataReducer);

  const [searchInput, setSearchInput] = React.useState('');

  useEffect(() => {
    return () =>
      dispatch({
        type: actions.SEARCH_FAMILY_DATA_RESET
      });
  }, []);

  const classes = useStyles();

  const handleSearchInputChange = event => {
    dispatch({
      type: actions.SEARCH_FAMILY_DATA,
      payload: event.target.value
    });
    setSearchInput(event.target.value);
  };

  const handleSearchResultItemSelect = familyInfo => {
    props.handleViewOfFamilyData(familyInfo);
  };

  return (
    <div className={classes.container}>
      <form noValidate autoComplete="off" className={classes.form}>
        <TextField
          id="outlined-search"
          type="search"
          className={classes.textField}
          margin="normal"
          variant="outlined"
          value={searchInput}
          onChange={handleSearchInputChange}
          placeholder={'Search users in db...'}
        />
      </form>
      {searchedFamilyData.length > 0 ? (
        <List component="nav">
          {searchedFamilyData.map(familyData => (
            <ListItem
              button
              key={familyData.uniqueCode}
              onClick={() => handleSearchResultItemSelect(familyData)}
            >
              <ListItemText primary={familyData.primaryMail} />
            </ListItem>
          ))}
        </List>
      ) : (
        searchInput !== '' && (
          <List component="nav">
            <ListItem>
              <ListItemText primary={'No Records Found. '} />
              <ListItemIcon onClick={() => props.handleStartTree()}>
                <AddIcon />
              </ListItemIcon>
            </ListItem>
          </List>
        )
      )}
    </div>
  );
};

export default SearchPage;

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PersonCard from './PersonCard';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FatherIcon from './static/images/fatherIcon.png';
import MotherIcon from './static/images/motherIcon.png';

const useStyles = makeStyles({
  container: {
    display: 'inline-flex',
    flexDirection: 'column',
    borderTop: '10px solid orange',
    borderRadius: '50%',
    margin: 10,
    alignItems: 'center'
  },
  containerChild: {
    display: 'flex'
    // alignItems: 'center',
  },
  familyContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  flexContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    marginTop: 0,
    padding: 10,
    boxShadow:
      '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)'
  },
  flexContainerColum: {
    display: 'flex',
    flexDirection: 'column'
  },
  verticalLine: {
    height: 80,
    border: '5px solid maroon'
  },
  heartIcon: {
    float: 'left',
    color: 'red',
    marginTop: '40%',
    paddingRight: '4%'
  },
  fatherIcon: {
    float: 'left'
  },
  motherIcon: {
    float: 'left'
  }
});

const LevelInfo = props => {
  const classes = useStyles();
  const {
    handleSpouse,
    handleChildren,
    handleParent,
    familyTreeData,
    cardClickedForAddingEditingInfo,
    deleteInfo,
    ids,
    hideChildren,
    userAction
  } = { ...props };

  return ids.map(id => (
    <>
      <div className={classes.container}>
        <div className={classes.familyContainer} key={`person-${id}-family`}>
          {id !== 'A1' && <div className={classes.verticalLine}></div>}
          <div className={classes.flexContainer}>
            <div>
              <PersonCard
                key={`person-${id}-child`}
                handleSpouse={handleSpouse}
                handleChildren={handleChildren}
                handleParent={handleParent}
                cardClickedForAddingEditingInfo={cardClickedForAddingEditingInfo}
                deleteInfo={deleteInfo}
                hideChildren={hideChildren}
                personinfo={familyTreeData[id]}
                userAction={userAction}
              />
            </div>
            {familyTreeData[`${id}-spouse`] !== undefined && (
              <div>
                <FavoriteIcon className={classes.heartIcon} />
                <PersonCard
                  key={`person-${id}-spouse`}
                  handleSpouse={handleSpouse}
                  handleChildren={handleChildren}
                  handleParent={handleParent}
                  cardClickedForAddingEditingInfo={cardClickedForAddingEditingInfo}
                  deleteInfo={deleteInfo}
                  hideChildren={hideChildren}
                  personinfo={familyTreeData[`${id}-spouse`]}
                  userAction={userAction}
                />
              </div>
            )}
            <span className={classes.flexContainerColum}>
              {familyTreeData[`${id}-spouse-father`] !== undefined && (
                <div>
                  <img src={FatherIcon} className={classes.fatherIcon} alt={'father_icon'} />
                  <PersonCard
                    key={`person-${id}-spouse-father`}
                    handleSpouse={handleSpouse}
                    handleChildren={handleChildren}
                    handleParent={handleParent}
                    cardClickedForAddingEditingInfo={cardClickedForAddingEditingInfo}
                    deleteInfo={deleteInfo}
                    hideChildren={hideChildren}
                    personinfo={familyTreeData[`${id}-spouse-father`]}
                    userAction={userAction}
                  />
                </div>
              )}
              {familyTreeData[`${id}-spouse-mother`] !== undefined && (
                <div>
                  <img src={MotherIcon} className={classes.motherIcon} alt={'mother_icon'} />
                  <PersonCard
                    key={`person-${id}-spouse-mother`}
                    handleSpouse={handleSpouse}
                    handleChildren={handleChildren}
                    handleParent={handleParent}
                    cardClickedForAddingEditingInfo={cardClickedForAddingEditingInfo}
                    deleteInfo={deleteInfo}
                    hideChildren={hideChildren}
                    personinfo={familyTreeData[`${id}-spouse-mother`]}
                    userAction={userAction}
                  />
                </div>
              )}
            </span>
          </div>
        </div>
        {familyTreeData[id].children && familyTreeData[id].showChildren && (
          <div className={classes.containerChild}>
            <LevelInfo
              handleSpouse={handleSpouse}
              handleChildren={handleChildren}
              handleParent={handleParent}
              cardClickedForAddingEditingInfo={cardClickedForAddingEditingInfo}
              deleteInfo={deleteInfo}
              hideChildren={hideChildren}
              familyTreeData={familyTreeData}
              ids={familyTreeData[id].children}
              userAction={userAction}
            />
          </div>
        )}
      </div>
    </>
  ));
};

export default LevelInfo;

import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import CardMedia from '@material-ui/core/CardMedia';
import Clear from '@material-ui/icons/Clear';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import * as R from 'ramda';
import { doPatternTestForInputField } from './utils/GeneralUtils';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  flexItem: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '8px'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: '40%'
  },
  dob: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: '45%'
  },
  nameField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: '35%'
  },
  textArea: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: '100%'
  },
  formControl: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    minWidth: '10%',
    maxWidth: '40%'
  },
  bloodGroup: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '40%'
  },
  media_profile_pic: {
    height: 100,
    width: 100
  },
  profilePicSection: {
    border: '1px solid'
  }
}));

const PersonDetails = props => {
  const [firstName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [mobile, setMobile] = React.useState('');
  const [title, setTitle] = React.useState('');
  const [bloodGroup, setBloodGroup] = React.useState('');
  const [otherBloodGroup, setOtherBloodGroup] = React.useState('');
  const [educationQualification, setEducationalQualification] = React.useState('');
  const [occupation, setOccupation] = React.useState('');
  const [occupationAddress, setOccupationAddress] = React.useState('');
  const [address, setAddress] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [native, setNative] = React.useState('');
  const [natureOfBusiness, setNatureOfBusiness] = React.useState('');
  const [dob, setDob] = React.useState(null);
  const [image, setImage] = React.useState(null);
  const [profilePic, setProfilePic] = React.useState(null);
  const [saveEnabled, setSaveEnabled] = React.useState(true);
  const [error, setError] = React.useState({
    firstName: false,
    lastName: false,
    title: false,
    mobile: false,
    address: false,
    email: false,
    occupationAddress: false,
    occupation: false,
    otherBloodGroup: false,
    educationQualification: false,
    bloodGroup: false,
    native: false,
    natureOfBusiness: false
  });

  useEffect(() => {
    if (props.personDetails !== undefined) {
      setFirstName(props.personDetails.firstName);
      setLastName(props.personDetails.lastName);
      setTitle(props.personDetails.title);
      setMobile(props.personDetails.mobile);
      setBloodGroup(props.personDetails.otherBloodGroup);
      setOtherBloodGroup(props.personDetails.mobile);
      setEducationalQualification(props.personDetails.educationQualification);
      setOccupation(props.personDetails.occupation);
      setOccupationAddress(props.personDetails.occupationAddress);
      setAddress(props.personDetails.address);
      setEmail(props.personDetails.email);
      setNative(props.personDetails.native);
      setNatureOfBusiness(props.personDetails.natureOfBusiness);
      setDob(props.personDetails.dob);
      setProfilePic(props.personDetails.profilePic);
    }
    // eslint-disable-next-line
  }, []);

  const classes = useStyles();

  const handleClose = () => {
    let personDetails = {
      firstName: error.firstName ? '' : firstName,
      lastName: error.lastName ? '' : lastName,
      title: error.title ? '' : title,
      mobile: error.mobile ? '' : mobile,
      address: error.address ? '' : address,
      email: error.email ? '' : email,
      occupationAddress: error.occupationAddress ? '' : occupationAddress,
      occupation: error.occupation ? '' : occupation,
      otherBloodGroup: error.otherBloodGroup ? '' : otherBloodGroup,
      educationQualification: error.educationQualification ? '' : educationQualification,
      bloodGroup: error.bloodGroup ? '' : bloodGroup,
      natureOfBusiness: error.natureOfBusiness ? '' : natureOfBusiness,
      native: error.native ? '' : native,
      dob,
      profilePic: props.personDetails && props.personDetails.profilePic,
      status: 0
    };
    props.handleSave(props.personId, { personDetails });
  };

  const handleReset = () => {
    setFirstName('');
    setLastName('');
    setTitle('');
    setMobile('');
    setBloodGroup('');
    setOtherBloodGroup('');
    setEducationalQualification('');
    setOccupation('');
    setOccupationAddress('');
    setAddress('');
    setEmail('');
    setNative('');
    setNatureOfBusiness('');
    setDob(null);
    setError({
      firstName: false,
      lastName: false,
      title: false,
      mobile: false,
      address: false,
      email: false,
      occupationAddress: false,
      occupation: false,
      otherBloodGroup: false,
      educationQualification: false,
      bloodGroup: false,
      native: false,
      natureOfBusiness: false
    });
    setSaveEnabled(true);
    setImage(null);
    setProfilePic(null);
  };

  const handleSave = () => {
    let personDetails = {
      firstName,
      lastName,
      title,
      mobile,
      address,
      email,
      occupationAddress,
      occupation,
      otherBloodGroup,
      educationQualification,
      bloodGroup,
      native,
      natureOfBusiness,
      dob,
      profilePic,
      status: 1
    };
    props.handleSave(props.personId, { personDetails, image });
  };

  const pattern = {
    firstName: '^[A-Za-z_.-]+$',
    lastName: '^[A-Za-z_.-]+$',
    mobile: '^[0-9]*$',
    email: '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$',
    occupation: '^[A-Za-z _.-]+$',
    otherBloodGroup: '^[A-Za-z_.-]+$',
    educationQualification: '^[A-Za-z _.-]+$',
    native: '^[A-Za-z _.-]+$',
    natureOfBusiness: '^[A-Za-z _.-]+$'
  };

  const requiredFields = [
    'firstName',
    'lastName',
    'title',
    'mobile',
    'address',
    'email',
    'occupation',
    'native',
    'dob'
  ];

  const handleOnBlur = event => {
    let errorCopy = R.clone(error);
    if (R.contains(event.target.id, requiredFields)) {
      if (event.target.value.length > 0) {
        let validInput = doPatternTestForInputField(event, pattern[event.target.id]);
        errorCopy[event.target.id] = !validInput;
      } else {
        errorCopy[event.target.id] = true;
      }
    } else {
      if (event.target.value.length > 0) {
        let validInput = doPatternTestForInputField(event, pattern[event.target.id]);
        errorCopy[event.target.id] = !validInput;
      }
    }
    setError(errorCopy);
    setSaveEnabled(R.isEmpty(R.pickBy((v, k) => v, errorCopy)));
  };

  return (
    <Dialog
      open={props.open}
      onClose={handleClose}
      aria-labelledby="person-details-dialog-title"
      aria-describedby="person-details-dialog-description"
      maxWidth={'md'}
      fullWidth={true}
    >
      <DialogTitle id="person-details-dialog-title">
        Personal Details:{' '}
        {props.personDetails !== undefined
          ? `${props.personDetails.title} ${props.personDetails.firstName} ${props.personDetails.lastName}`
          : props.personId}
      </DialogTitle>
      <DialogContent>
        <form className={classes.container} autoComplete="off">
          <div className={classes.flexItem}>
            <FormControl
              required
              className={classes.formControl}
              disabled={props.userAction === 'view'}
            >
              <InputLabel id="title_label">Title</InputLabel>
              <Select
                labelId="title_label"
                value={title}
                className={classes.titleField}
                onChange={event => setTitle(event.target.value)}
              >
                <MenuItem value={'Mr'}>Mr.</MenuItem>
                <MenuItem value={'Mrs'}>Mrs.</MenuItem>
                <MenuItem value={'Miss'}>Miss</MenuItem>
                <MenuItem value={'Lt'}>Lt</MenuItem>
              </Select>
            </FormControl>
            <TextField
              id="firstName"
              required
              error={error['firstName']}
              className={classes.nameField}
              label="First Name"
              margin="normal"
              value={firstName}
              onChange={event => setFirstName(event.target.value)}
              onBlur={e => handleOnBlur(e)}
              onKeyUp={e => handleOnBlur(e)}
              disabled={props.userAction === 'view'}
              autoComplete="off"
              helperText={
                error['firstName'] && firstName.length !== 0 ? 'Should contain alphabets' : ''
              }
            />
            <TextField
              id="lastName"
              required
              error={error['lastName']}
              className={classes.nameField}
              label="Last Name"
              margin="normal"
              value={lastName}
              onChange={event => setLastName(event.target.value)}
              onBlur={e => handleOnBlur(e)}
              onKeyUp={e => handleOnBlur(e)}
              disabled={props.userAction === 'view'}
              autoComplete="off"
              helperText={
                error['firstName'] && lastName.length !== 0 ? 'Should contain alphabets' : ''
              }
            />
          </div>
          <div className={classes.flexItem}>
            <TextField
              id="mobile"
              required
              error={error['mobile']}
              className={classes.textField}
              label="Mobile"
              margin="normal"
              value={mobile}
              onChange={event => setMobile(event.target.value)}
              onBlur={e => handleOnBlur(e)}
              onKeyUp={e => handleOnBlur(e)}
              disabled={props.userAction === 'view'}
              autoComplete="off"
              helperText={error['mobile'] && mobile.length !== 0 ? 'Should contain numbers' : ''}
            />
            <TextField
              id="email"
              required
              error={error['email']}
              className={classes.textField}
              label="Email"
              margin="normal"
              value={email}
              onChange={event => setEmail(event.target.value)}
              onBlur={e => handleOnBlur(e)}
              onKeyUp={e => handleOnBlur(e)}
              disabled={props.userAction === 'view'}
              autoComplete="off"
              helperText={error['email'] && email.length !== 0 ? 'Should contain valid email' : ''}
            />
          </div>
          <div className={classes.flexItem}>
            <MuiPickersUtilsProvider utils={MomentUtils}>
              <KeyboardDatePicker
                variant="inline"
                required
                format="DD/MM/YYYY"
                margin="normal"
                className={classes.dob}
                id="dob"
                label="Date Of Birth"
                value={dob}
                onChange={date => setDob(date)}
                onBlur={e => handleOnBlur(e)}
                onKeyUp={e => handleOnBlur(e)}
                KeyboardButtonProps={{
                  'aria-label': 'change date'
                }}
              />
            </MuiPickersUtilsProvider>
          </div>
          <div className={classes.flexItem}>
            <TextField
              id="address"
              required
              label="Address"
              multiline
              rows="4"
              value={address}
              className={classes.textArea}
              margin="normal"
              onChange={event => setAddress(event.target.value)}
              disabled={props.userAction === 'view'}
              autoComplete="off"
            />
          </div>
          <div className={classes.flexItem}>
            <FormControl className={classes.bloodGroup} disabled={props.userAction === 'view'}>
              <InputLabel id="blood_group_label">Blood Group</InputLabel>
              <Select
                labelId="blood_group_label"
                value={bloodGroup}
                className={classes.titleField}
                onChange={event => setBloodGroup(event.target.value)}
              >
                <MenuItem value={'A-'}>A-ve</MenuItem>
                <MenuItem value={'A+'}>A+ve</MenuItem>
                <MenuItem value={'B-'}>B-ve</MenuItem>
                <MenuItem value={'B+'}>B+ve</MenuItem>
                <MenuItem value={'AB-'}>AB-ve</MenuItem>
                <MenuItem value={'AB+'}>AB+ve</MenuItem>
                <MenuItem value={'O-'}>O-ve</MenuItem>
                <MenuItem value={'O+'}>O+ve</MenuItem>
                <MenuItem value={'Oth'}>Other</MenuItem>
              </Select>
            </FormControl>
            {bloodGroup === 'Oth' && (
              <TextField
                id="otherBloodGroup"
                required
                error={error['otherBloodGroup']}
                className={classes.textField}
                label="Other Blood Group"
                margin="normal"
                value={otherBloodGroup}
                onChange={event => setOtherBloodGroup(event.target.value)}
                onBlur={e => handleOnBlur(e)}
                onKeyUp={e => handleOnBlur(e)}
                disabled={props.userAction === 'view'}
                autoComplete="off"
                helperText={
                  error['otherBloodGroup'] && otherBloodGroup.length !== 0
                    ? 'Should contain alphabets'
                    : ''
                }
              />
            )}
          </div>
          <div className={classes.flexItem}>
            <TextField
              id="native"
              required
              error={error['native']}
              className={classes.textField}
              label="Native"
              margin="normal"
              value={native}
              onChange={event => setNative(event.target.value)}
              onBlur={e => handleOnBlur(e)}
              onKeyUp={e => handleOnBlur(e)}
              disabled={props.userAction === 'view'}
              autoComplete="off"
              helperText={error['native'] && native.length !== 0 ? 'Should contain alphabets' : ''}
            />
            <TextField
              id="educationalQualification"
              error={error['educationalQualification']}
              className={classes.textField}
              label="Educational Qualification"
              margin="normal"
              value={educationQualification}
              onChange={event => setEducationalQualification(event.target.value)}
              onBlur={e => handleOnBlur(e)}
              onKeyUp={e => handleOnBlur(e)}
              disabled={props.userAction === 'view'}
              autoComplete="off"
              helperText={
                error['educationalQualification'] && educationQualification.length !== 0
                  ? 'Should contain alphabets'
                  : ''
              }
            />
          </div>
          <div className={classes.flexItem}>
            <TextField
              id="occupation"
              required
              error={error['occupation']}
              className={classes.textField}
              label="Occupation"
              margin="normal"
              value={occupation}
              onChange={event => setOccupation(event.target.value)}
              onBlur={e => handleOnBlur(e)}
              onKeyUp={e => handleOnBlur(e)}
              disabled={props.userAction === 'view'}
              autoComplete="off"
              helperText={error['occupation'] ? 'Should contain alphabets' : ''}
            />
            <TextField
              id="natureOfBusiness"
              error={error['natureOfBusiness']}
              className={classes.textField}
              label="Nature Of Business"
              margin="normal"
              value={natureOfBusiness}
              onChange={event => setNatureOfBusiness(event.target.value)}
              onBlur={e => handleOnBlur(e)}
              onKeyUp={e => handleOnBlur(e)}
              disabled={props.userAction === 'view'}
              autoComplete="off"
              helperText={
                error['natureOfBusiness'] && natureOfBusiness.length !== 0
                  ? 'Should contain alphabets'
                  : ''
              }
            />
          </div>
          <div className={classes.flexItem}>
            <TextField
              id="occupationAddress"
              label="Occupation Address"
              multiline
              rows="4"
              value={occupationAddress}
              className={classes.textArea}
              margin="normal"
              onChange={event => setOccupationAddress(event.target.value)}
              disabled={props.userAction === 'view'}
              autoComplete="off"
            />
          </div>
          <div className={classes.flexItem}>
            {!profilePic ? (
              <div>
                <label>Upload Image</label>
                <input
                  type="file"
                  name="file"
                  accept="image/*"
                  onChange={event => setImage(event.target.files[0])}
                  disabled={props.userAction === 'view'}
                />
              </div>
            ) : (
              <div className={classes.profilePicSection}>
                <Clear className={'delete-icon'} onClick={() => setProfilePic(null)} />
                <img
                  className={classes.media_profile_pic}
                  src={`${process.env.REACT_APP_URL}/profile_pics/${profilePic}`}
                  alt={
                    props.personDetails !== undefined
                      ? `${props.personDetails.title} ${props.personDetails.firstName} ${props.personDetails.lastName}`
                      : props.personId
                  }
                />
              </div>
            )}
          </div>
        </form>
      </DialogContent>
      {props.userAction !== 'view' && (
        <DialogActions>
          <Button onClick={handleReset} color="primary">
            RESET
          </Button>
          <Button onClick={handleSave} color="primary" autoFocus disabled={!saveEnabled}>
            SAVE
          </Button>
        </DialogActions>
      )}
    </Dialog>
  );
};

export default PersonDetails;

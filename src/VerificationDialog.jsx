import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: 0,
    width: 200
  },
  errorMessage: {
    color: 'red'
  }
}));

const VerificationDialog = props => {
  const [primaryMail, setPrimaryMail] = React.useState('');
  const [uniqueCode, setUniqueCode] = React.useState('');
  const [verificationMessage, setVerificationMessage] = React.useState('');

  const classes = useStyles();

  const handleReset = () => {
    setPrimaryMail('');
    setUniqueCode('');
  };

  const handleVerification = () => {
    if (
      primaryMail === props.verificationDetails.primaryMail &&
      uniqueCode === props.verificationDetails.uniqueCode
    )
      props.handleVerification();
    else setVerificationMessage('Invalid Details...Try again');
  };
  return (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{'Verification Details'}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Please enter the primary email and unique code for editing the information
        </DialogContentText>
        <form className={classes.container} autoComplete="off">
          <div>
            <TextField
              id="primaryMail"
              className={classes.textField}
              label="Primary Email"
              margin="normal"
              value={primaryMail}
              onChange={event => setPrimaryMail(event.target.value)}
            />
          </div>
          <div>
            <TextField
              id="uniqueCode"
              className={classes.textField}
              label="Unique Code"
              margin="normal"
              value={uniqueCode}
              onChange={event => setUniqueCode(event.target.value)}
            />
          </div>
        </form>
        {verificationMessage !== '' && (
          <span className={classes.errorMessage}>{verificationMessage}</span>
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleReset} color="primary">
          RESET
        </Button>
        <Button
          onClick={handleVerification}
          color="primary"
          autoFocus
          disabled={primaryMail === '' || uniqueCode === ''}
        >
          VERIFY
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default VerificationDialog;

import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from './history';
import MyLoadingComponent from './MyLoadingComponent';
import Loadable from 'react-loadable';

const AsyncHome = Loadable({
  loader: () => import(/* webpackChunkName: "App" */ './App'),
  loading: MyLoadingComponent
});

export default props => (
  <Router history={history}>
    <Switch>
      <Route exact path="/" component={AsyncHome} />
    </Switch>
  </Router>
);

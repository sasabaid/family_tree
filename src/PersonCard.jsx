import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import male_avatar from './static/images/male_avatar.jpg';
import female_avatar from './static/images/female_avatar.jpg';
import father_avatar from './static/images/father.png';
import mother_avatar from './static/images/mother.png';
import Typography from '@material-ui/core/Typography';
import Clear from '@material-ui/icons/Clear';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles({
  card: {
    maxWidth: 200,
    minWidth: 100,
    margin: 10,
    boxShadow: 'none',
    border: '1px solid'
  },
  media: {
    height: 50,
    width: 50,
    borderRadius: '50%',
    marginTop: '1%',
    marginLeft: '34%'
  },
  content: {
    padding: 0
  },
  card_father_mother: {
    width: 300,
    height: 'auto',
    margin: 10,
    padding: 10,
    display: 'flex',
    boxShadow: 'none',
    border: '1px solid'
  },
  media_father_mother: {
    height: 100,
    width: 100,
    borderRadius: '50%'
  },
  details_father_mother: {
    display: 'flex',
    flexDirection: 'column'
  },
  content_father_mother: {
    flex: '1 0 auto'
  }
});

const PersonCard = props => {
  const classes = useStyles();
  const { personinfo, userAction } = { ...props };
  const personalDetails = personinfo.personalDetails;
  return personinfo.type === 'father' || personinfo.type === 'mother' ? (
    <Card className={classes.card_father_mother}>
      <CardMedia
        className={classes.media_father_mother}
        image={personinfo.gender === 'male' ? father_avatar : mother_avatar}
        title={`${personinfo.gender === 'male' ? 'Male' : 'Female'} Avatar`}
        onClick={() => props.cardClickedForAddingEditingInfo(personinfo.id)}
      />
      <div className={classes.details_father_mother}>
        <CardContent
          className={classes.content_father_mother}
          onClick={() => props.cardClickedForAddingEditingInfo(personinfo.id)}
        >
          <Typography component="h5" variant="h5">
            {personalDetails !== undefined && personalDetails.firstName.length > 0
              ? `${personalDetails.title} ${personalDetails.firstName} ${personalDetails.lastName}`
              : personinfo.id}
          </Typography>
          <Typography variant="subtitle1" color="textSecondary">
            {personalDetails !== undefined && personalDetails.mobile.length > 0
              ? personalDetails.mobile
              : personinfo.level}
          </Typography>
        </CardContent>
      </div>
      {userAction !== 'view' && (
        <Clear className={'delete-icon'} onClick={() => props.deleteInfo(personinfo.id)} />
      )}
    </Card>
  ) : (
    <Card className={classes.card}>
      {(personinfo.type === 'children' || personinfo.type === 'head') &&
        (personinfo.showChildren ? (
          <VisibilityIcon
            className={'visiblity-icon'}
            onClick={() => props.hideChildren(personinfo.id)}
          />
        ) : (
          <VisibilityOffIcon
            className={'visiblity-icon'}
            onClick={() => props.hideChildren(personinfo.id)}
          />
        ))}
      {userAction !== 'view' && (
        <Clear className={'delete-icon'} onClick={() => props.deleteInfo(personinfo.id)} />
      )}
      <CardMedia
        className={classes.media}
        image={personinfo.gender === 'male' ? male_avatar : female_avatar}
        title={`${personinfo.gender === 'male' ? 'Male' : 'Female'} Avatar`}
        onClick={() => props.cardClickedForAddingEditingInfo(personinfo.id)}
      />
      <CardContent
        className={classes.content}
        onClick={() => props.cardClickedForAddingEditingInfo(personinfo.id)}
      >
        <Typography component="h5" variant="h5">
          {personalDetails !== undefined && personalDetails.firstName.length > 0
            ? `${personalDetails.title} ${personalDetails.firstName} ${personalDetails.lastName}`
            : personinfo.id}
        </Typography>
        <Typography variant="subtitle1" color="textSecondary">
          {personalDetails !== undefined && personalDetails.mobile.length > 0
            ? personalDetails.mobile
            : personinfo.level}
        </Typography>
      </CardContent>
      {userAction !== 'view' && (personinfo.type === 'children' || personinfo.type === 'head') && (
        <CardActions>
          <Button onClick={() => props.handleChildren(personinfo.id)} size="small" color="primary">
            Children
          </Button>
          <Button
            onClick={() => props.handleSpouse(personinfo.gender, personinfo.id)}
            size="small"
            color="primary"
            disabled={personinfo.spouse !== undefined}
          >
            Spouse
          </Button>
        </CardActions>
      )}
      {userAction !== 'view' && personinfo.type === 'spouse' && (
        <CardActions>
          <Button
            onClick={() => props.handleParent('male', personinfo.id)}
            size="small"
            color="primary"
            disabled={personinfo.father !== undefined}
          >
            Father
          </Button>
          <Button
            onClick={() => props.handleParent('female', personinfo.id)}
            size="small"
            color="primary"
            disabled={personinfo.mother !== undefined}
          >
            Mother
          </Button>
        </CardActions>
      )}
    </Card>
  );
};

export default PersonCard;

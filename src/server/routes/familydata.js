const express = require('express');
const router = express.Router();
const FamilyData = require('../models/familyData');

router.get('/', (req, res) => {
  FamilyData.find()
    .then(allFamilies => res.json(allFamilies))
    .catch(err => res.json({ message: err }));
});

router.post('/', async (req, res) => {
  const familyData = new FamilyData(req.body);
  try {
    const savedFamilyData = await familyData.save();
    res.json(savedFamilyData);
  } catch (err) {
    res.json({ message: err });
  }
});

router.get('/:familyId', (req, res) => {
  if (req.query.searchData !== undefined) {
    let searchString = req.query.searchData;
    FamilyData.find({ $text: { $search: searchString } })
      .then(foundFamilies => res.json(foundFamilies))
      .catch(err => res.json({ message: err }));
  } else {
    FamilyData.findOne({ _id: req.params.familyId })
      .then(foundFamily => res.json(foundFamily))
      .catch(err => res.json({ message: err }));
  }
});

router.delete('/:familyId', (req, res) => {
  FamilyData.deleteOne({ _id: req.params.familyId })
    .then(data => res.json(data))
    .catch(err => res.json({ message: err }));
});

router.put('/:familyId', (req, res) => {
  FamilyData.update({ _id: req.params.familyId }, { $set: req.body })
    .then(data => res.json(data))
    .catch(err => res.json({ message: err }));
});

module.exports = router;

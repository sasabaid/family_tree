require('dotenv').config();

const homePageRedirect = (req, res) => {
  res.redirect('/');
};

const authFailure = function(req, res) {
  res.send({ error: 'Failed to authenticate' });
};

const userAPIProxySettings = {
  target: process.env.USER_API,
  changeOrigin: true,
  ws: true,
  secure: false,
  pathRewrite(path, req) {
    return path.replace(/api\/user/, '/api/user');
  }
};

// const postAPIProxySettings = {
//   target: process.env.API_URL,
//   changeOrigin: true,
//   ws: true,
//   secure: false,
//   pathRewrite(path, req) {
//     return path.replace('', '');
//   },
//   onProxyReq: function onProxyReq(proxyReq, req, res) {
//     if (req.method === 'POST') {
//       if (req.body) {
//         let bodyData = JSON.stringify(req.body);
//         proxyReq.setHeader('Content-Type', 'application/json');
//         proxyReq.write(bodyData);
//       }
//     }
//   },
// };

// const putAPIProxySettings = {
//   target: process.env.API_URL,
//   changeOrigin: true,
//   ws: true,
//   secure: false,
//   pathRewrite(path, req) {
//     return path.replace('', '');
//   },
//   onProxyReq: function onProxyReq(proxyReq, req, res) {
//     if (req.method === 'PUT') {
//       if (req.body) {
//         let bodyData = JSON.stringify(req.body);
//         proxyReq.setHeader('Content-Type', 'application/json');
//         proxyReq.write(bodyData);
//       }
//     }
//   },
// };

exports.homePageRedirect = homePageRedirect;
exports.authFailure = authFailure;
exports.userAPIProxySettings = userAPIProxySettings;

const mongoose = require('mongoose');

const FamilyDataSchema = mongoose.Schema({
  familyTreeData: {
    type: mongoose.Mixed,
    required: true
  },
  noOfPerson: {
    type: Number,
    required: true
  },
  primaryMail: {
    type: String,
    required: true
  },
  uniqueCode: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

FamilyDataSchema.index({ '$**': 'text' });

module.exports = mongoose.model('FamilyData', FamilyDataSchema);

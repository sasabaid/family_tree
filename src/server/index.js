const express = require('express');
const path = require('path');
const proxy = require('http-proxy-middleware');
const expressSession = require('express-session');
const logger = require('./helpers/logger');
const appRoutes = require('./app-routes');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const fakeResponse = require('./fakeChipResponse');
const correlator = require('express-correlation-id');
const ping = require('./routes/ping');
const familyDataRoutes = require('./routes/familydata');
const mongoose = require('mongoose');
const multer = require('multer');
require('dotenv/config');

const PORT = process.env.PORT || 8000;

const app = express();
app.use(
  expressSession({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
  })
);

app.use(logger.winston);
app.use(logger.express);
app.use(cookieParser());
app.use(correlator());
const corrMiddleware = function(req, res, next) {
  req.headers['X-Correlation-ID'] = req.correlationId();
  next();
};

app.use(corrMiddleware);
app.use(express.static(path.join('build')));
app.use(express.static(path.join('public')));

app.use((req, res, next) => {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
});

app.use(bodyParser.json({ limit: '256kb' }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: '256kb'
  })
);

let storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, './public/profile_pics');
  },
  filename: (req, file, callback) => {
    callback(null, file.originalname);
  }
});

let upload = multer({
  storage: storage,
  fileFilter: (req, file, callback) => {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
      callback(null, false);
      callback(new Error('Only Image Files allowed'));
    } else {
      callback(null, true);
    }
  }
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../../build', 'index.html'));
});

app.use(proxy(['**/api/user**'], appRoutes.userAPIProxySettings));

app.use('/api/ping', ping);

app.use('/familydata', familyDataRoutes);

app.get('/api/states', (req, res) => {
  res.send(fakeResponse.data.states);
});

let uploadFile = upload.single('file');

let uploadFileCheck = (req, res, next) => {
  uploadFile(req, res, err => {
    if (err) {
      res.statusCode = 400;
      res.json({ message: err.message });
    } else {
      next();
    }
  });
};

app.post('/uploadProfilePic', uploadFileCheck, (req, res) => {
  const file = req.file;
  if (!file) {
    const error = new Error('Error in uploading file');
    res.statusCode = 400;
    res.json({ message: error.message });
  }
  res.send(file);
});

mongoose.connect(process.env.DB_CONNECTION_URL, { useUnifiedTopology: true }, () =>
  console.log('Connected to DB!')
);

app.listen(PORT, error => {
  if (error) {
    console.error(error);
  }
  console.info(
    `==> 🌎 App Listening on ${PORT} please open your browser and navigate to http://localhost:${PORT}/`
  );
});
